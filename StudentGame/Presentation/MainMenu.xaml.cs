﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace StudentGame.Presentation
{
    

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainMenu : Page
    {
        private Player _player;

        public MainMenu()
        {

            this.InitializeComponent();

        }

        private void textBox_TextChanged(System.Object sender, TextChangedEventArgs e)
        {

        }

        /// <summary>
        /// Navigates to new character creation or selection, will only navigate to character creation immediately if no save files are located
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnStartGame(System.Object sender, RoutedEventArgs e)
        {
            //TODO: add if statement so that it checks if every saved file spot is full, preventing more than 3 saved characters
            this.Frame.Navigate(typeof(CharacterCreation));


        }

            /// <summary>
            /// Navigates to Highscores
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
        private void OnOpenHighScores(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(HighScores));
        }

        /// <summary>
        /// Exits the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnExit(object sender, RoutedEventArgs e)
        {
            MessageDialog msgDlg = new MessageDialog("Are you sure you want to exit?");
            msgDlg.Commands.Add(new UICommand("Yes",
                new UICommandInvokedHandler(ExitProgram)));//TODO: implement exit
            msgDlg.Commands.Add(new UICommand("Cancel"));

            await msgDlg.ShowAsync();
            // MessageDialog msgDlg = new MessageDialog("TODO: Ask user if they want to exit or not");
            //await msgDlg.ShowAsync();
        }

        private void ExitProgram(IUICommand command)
        {
            // Exits the program
            
        }
    }
}
