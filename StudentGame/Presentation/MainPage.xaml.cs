﻿using StudentGame.Presentation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409


namespace StudentGame { 
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Shop _shop;

        private Game _game;

        private Player _player;

        private Enemy _enemy;

        private List<Enemy> _enemList;

        static Random rng = new Random();

        byte _stageNumber = 1;

        DispatcherTimer _walkTimer = new DispatcherTimer();
        DispatcherTimer _attackTimer = new DispatcherTimer();



        public MainPage()
        {
            this.InitializeComponent();
            _game = new StudentGame.Game();
            _enemList = new List<Enemy>();

            // Player needs to be created either here or in character creation
            _player = new StudentGame.Player("Student");
            _playerName.Text = _player.EntityName;
            _txtHealth.Text = _player.EntityHealth.ToString();
            _txtLevel.Text = _player.Level.ToString();
            _txtAttack.Text = _player.MinimumDamage.ToString() + "-" + _player.MaximumDamage.ToString();
            _txtDefense.Text = _player.Defence.ToString();

            

            _btnStage1.IsEnabled = false;
            _btnStage2.IsEnabled = false;
            _btnStage3.IsEnabled = false;
            _btnStage4.IsEnabled = false;
            _btnStage5.IsEnabled = false;
            _btnStage6.IsEnabled = false;
            _btnStage7.IsEnabled = false;
            _btnStage8.IsEnabled = false;
            _btnStage9.IsEnabled = false;

            _walkTimer.Tick += WalkTimer_Tick;
            _walkTimer.Interval = TimeSpan.FromSeconds(1);

            _attackTimer.Tick += AttackTimer_Tick;
            _attackTimer.Interval = TimeSpan.FromSeconds(1);

            Start();



        }

        private void AttackTimer_Tick(object sender, object e)
        {
            throw new NotImplementedException();
        }

        private void WalkTimer_Tick(object sender, object e)
        {
            AddEnemy();
        }


        private void Start()
        {
            _btnStage2.IsEnabled = false;
            _btnStage3.IsEnabled = false;
            _btnStage4.IsEnabled = false;
            _btnStage5.IsEnabled = false;
            _btnStage6.IsEnabled = false;
            _btnStage7.IsEnabled = false;
            _btnStage8.IsEnabled = false;
            _btnStage9.IsEnabled = false;

            _walkTimer.Start();
            StartStage();





        }

        public void StartStage()
        {
            _enemList = _game.CreateEnemyList(_stageNumber);
            AddEnemy();
        }

        private void AddEnemy()
        {
            ContentControl enemy = new ContentControl();
            enemy.Template = Resources["testEnem"] as ControlTemplate;
            AnimateEnemy(enemy, _playAreaPnl.ActualWidth, 0, "(Canvas.Left)");
            if (_playAreaPnl.Children.Count < 2)
            {
                _playAreaPnl.Children.Add(enemy);
                _playerHealth.Text = _playAreaPnl.Children.Count.ToString();
            }

        }

        private void AnimateEnemy(ContentControl enemy, double from, double to, string p2)
        {
            Storyboard storyboard = new Storyboard() { AutoReverse = true };
            DoubleAnimation animation = new DoubleAnimation()
            {
                From = from,
                To = to,
                Duration = new Duration(TimeSpan.FromSeconds(rng.Next(4,6)))

            };

            Storyboard.SetTarget(animation,enemy);
            Storyboard.SetTargetProperty(animation,p2);
            storyboard.Children.Add(animation);
            storyboard.Begin();

        }

        private void OnStageEnabled()
        {
            if (_btnStage1.IsEnabled)
            {
                //
            }
        }

        private void OnBossDefeated()
        {

        }

        /// <summary>
        /// Event that allows user to attack confronted enemy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnAttack(System.Object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Event that allows user to open their inventory/bag
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnOpenBag(System.Object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Event that allows user to flee from battle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRun(System.Object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainMenu));
        }

        /// <summary>
        /// Event that allows user to open the store but only accessible out of combat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnOpenStore(System.Object sender, RoutedEventArgs e)
        {
          


            //if( //player stage)
        }
    }
}
