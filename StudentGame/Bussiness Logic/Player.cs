﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudentGame
{
    public class Player : Entity
    {

        static Random rng = new Random();

        private int _experienceRequirements;

        public int ExperienceRequirements
        {
            get
            {
                return _experienceRequirements;
            }

            set
            {
                _experienceRequirements = value;
            }
        }

        /// <summary>
        /// Constructor for player object
        /// </summary>
        public Player(string stuName) : base("", 0, 0, 0, 0, 0, 0, 0, 0, 0, null)
        {
            EntityName = stuName;
            EntityHealth = 100;
            GenerateStats(stuName);
            Defence = 5;
            Accuracy = 80;
            Experience = 0;
            Level = 0;
            Currency = 0;
            Luck = 10; // Will be changed later
            Inventory = new Inventory(1);
            ExperienceRequirements = 100;
        }

        /// <summary>
        /// Method for player generation, gets called when the instance of player object gets created.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public void GenerateStats(string name)
        {
            MinimumDamage = rng.Next(3, 6);
            MaximumDamage = rng.Next(4, 7) + MinimumDamage;
            while (MaximumDamage == MinimumDamage)
            {
                MaximumDamage = rng.Next(1, 3) + MinimumDamage;
            }
            return;
        }

        

        /// <summary>
        /// Method that returns all the inventory as a string
        /// </summary>
        /// <returns></returns>
        public string ShowInventoryContents()
        {
            return null;
        }


        /// <summary>
        /// Method that gets called once player levels up
        /// </summary>
        public void OnLevelUp()
        {
            // Updates the stats with new values
            EntityHealth += 10;
            MinimumDamage += 2 + (1 + rng.Next(0, Level));
            MaximumDamage += 3 + (1 + rng.Next(0, Level));
            Accuracy += 0.5;
            ExperienceRequirements *= 2;
            Level += 1;
            return;
        }

        /// <summary>
        /// Method that gets called once the player levels up
        /// </summary>
        /// <returns></returns>
        public string ShowLevelUpMessage()
        {
            if (Experience >= ExperienceRequirements)
            {
                return "You have leveled up! \nStats increased!";
            }
            return null;
        }

        /// <summary>
        /// Add method to save
        /// </summary>
        /// <param name="writer"></param>
        internal void Save(object writer)
        {
            //writer.WriteLine();
            
        }
    }
}
