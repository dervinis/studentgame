﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentGame
{
    public class Shop
    {
        static Random rng = new Random();
        private Inventory _inventory;
        private List<Item> _shopContents;
        private string[] _possibleItemType = new string[4] { "Pen", "Book", "TShirt", "Ruler" };
        private string[] _possibleItemSuffix = new string[4] { "Depression", "Life", "Suffering", "Acing" };
        /// <summary>
        /// Shop contents can get added after a stage
        /// </summary>
        private string _shopName;


        public Shop(string shopName)
        {
            _shopName = shopName;

        }


        public Item SellItem()
        {
            return null;
        }

        public void CreateShopContents(byte stageNumber)
        {
            for (int i = 0; i < 3; i++)
            {
                string combinedName = _possibleItemType[rng.Next(_possibleItemType.Length)] + " of " + _possibleItemSuffix[rng.Next(_possibleItemSuffix.Length)];
                int minAttackValue = stageNumber * 2 + (rng.Next(stageNumber, stageNumber * 2));
                int maxAttackValue = stageNumber * 2 + (rng.Next(stageNumber, stageNumber * 2 + 10)) + minAttackValue;
                while (maxAttackValue == minAttackValue)
                {
                    maxAttackValue = stageNumber * 2 + (rng.Next(stageNumber, stageNumber * 2 + 10)) + minAttackValue;
                }

                _shopContents.Add(new Item(combinedName, minAttackValue, maxAttackValue, 100));
            }
        }
    }
}
