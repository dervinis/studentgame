﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentGame
{
    public class Entity
    {
        private Inventory _inventory;

        /// <summary>
        /// Field variable for entityName
        /// </summary>
        private string _entityName;

        /// <summary>
        /// Field variable for entityHealth
        /// </summary>
        private int _entityHealth;

        /// <summary>
        /// Field variable for minimumDamage of an Entity
        /// </summary>
        private int _minimumDamage;

        /// <summary>
        /// Field variable for maximumDamage of an Entity
        /// </summary>
        private int _maximumDamage;

        /// <summary>
        /// Field variable for defence of an Entity
        /// </summary>
        private int _defence;

        /// <summary>
        /// Field variable for accuracy of an Enity
        /// </summary>
        private double _accuracy;

        /// <summary>
        /// Field variable for experience of an Entity
        /// </summary>
        private int _experience;

        /// <summary>
        /// Field variable for the level of an Entity
        /// </summary>
        private int _level;

        /// <summary>
        /// Field variable for the currency of an Entity;
        /// </summary>
        private float _currency;

        /// <summary>
        /// Field variable for the luck of an entity;
        /// </summary>
        private int _luck;



        /// <summary>
        /// Properties of the field variables
        /// </summary>
        public virtual string EntityName
        {
            get
            {
                return _entityName;
            }

            set
            {
                _entityName = value;
            }
        }

        public virtual int EntityHealth
        {
            get
            {
                return _entityHealth;
            }

            set
            {
                _entityHealth = value;
            }
        }

        public virtual int MinimumDamage
        {
            get
            {
                return _minimumDamage;
            }

            set
            {
                _minimumDamage = value;
            }
        }

        public virtual int MaximumDamage
        {
            get
            {
                return _maximumDamage;
            }

            set
            {
                _maximumDamage = value;
            }
        }

        public virtual int Experience
        {
            get
            {
                return _experience;
            }

            set
            {
                _experience = value;
            }
        }

        public virtual int Level
        {
            get
            {
                return _level;
            }

            set
            {
                _level = value;
            }
        }


        public float Currency
        {
            get
            {
                return _currency;
            }

            set
            {
                _currency = value;
            }
        }

        public double Accuracy
        {
            get
            {
                return _accuracy;
            }

            set
            {
                _accuracy = value;
            }
        }

        public int Defence
        {
            get
            {
                return _defence;
            }

            set
            {
                _defence = value;
            }
        }

        public int Luck
        {
            get
            {
                return _luck;
            }

            set
            {
                _luck = value;
            }
        }

        internal Inventory Inventory
        {
            get
            {
                return _inventory;
            }

            set
            {
                _inventory = value;
            }
        }



        /// <summary>
        /// A constructor for Entity object
        /// </summary>
        /// <param name="etName"></param>
        /// <param name="etHealth"></param>
        /// <param name="minDmg"></param>
        /// <param name="maxDmg"></param>
        /// <param name="exp"></param>
        /// <param name="lvl"></param>
        /// <param name="inv"></param>
        public Entity(string etName, int etHealth, int minDmg, int maxDmg, int def, double accu, int exp, int lvl, float curr, int lk, Inventory inv)
        {
            _entityName = etName;
            _entityHealth = etHealth;
            _minimumDamage = minDmg;
            _maximumDamage = maxDmg;
            _defence = def;
            _accuracy = accu;
            _experience = exp;
            _level = lvl;
            _currency = curr;
            _luck = lk;
            _inventory = inv;
        }

    }
}
