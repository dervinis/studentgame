﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudentGame
{
    public class Inventory
    {
        /// <summary>
        /// List of items in the inventory
        /// </summary>
        List<Item> _itemList;

        /// <summary>
        /// A random number generator that generates random numbers for the items
        /// </summary>
        static Random rng = new Random();

        /// <summary>
        /// A constructor for Inventory object
        /// </summary>
        /// 

        string[] _listItemType = new string[4] { "Pen", "Book", "TShirt", "Ruler" };
        string[] _listItemSuffix = new string[4] { "Depression", "Life", "Suffering", "Acing" };

        internal List<Item> ItemList
        {
            get
            {
                return _itemList;
            }

            set
            {
                _itemList = value;
            }
        }

        /// <summary>
        /// Constructor for Inventory object 
        /// </summary>
        /// <param name="setOfItem"></param>
        public Inventory(byte setOfItem)
        {
            _itemList = new List<Item>();
            if (setOfItem == 1) // Player inventory generation
            {
                Item item1 = GenerateItem();
                Item item2 = GenerateItem();
                while (item2.ItemName == item1.ItemName)
                {
                    item2 = GenerateItem();
                }
                Item item3 = GenerateItem();
                while (item3.ItemName == item1.ItemName || item3.ItemName == item2.ItemName)
                {
                    item3 = GenerateItem();
                }
                ItemList.Add(item1);
                ItemList.Add(item2);
                ItemList.Add(item3);
            }
            else if (setOfItem == 2) // Enemy inventory generation
            {
                Item item1 = GenerateItem();
                Item item2 = GenerateItem();
                while (item2.ItemName == item1.ItemName)
                {
                    item2 = GenerateItem();
                }
                ItemList.Add(item1);
                ItemList.Add(item2);
            }
            else if (setOfItem == 3) // Boss inventory generation
            {
                Item item1 = GenerateItem();
                Item item2 = GenerateItem();
                while (item2.ItemName == item1.ItemName)
                {
                    item2 = GenerateItem();
                }
                Item bossItem = GenerateBossItem();
                while (bossItem.ItemName == item1.ItemName || bossItem.ItemName == item2.ItemName)
                {
                    bossItem = GenerateBossItem();
                }
                ItemList.Add(item1);
                ItemList.Add(item2);
                ItemList.Add(bossItem);
            }
        }

        /// <summary>
        /// Method that generates boss items
        /// </summary>
        /// <returns></returns>
        private Item GenerateBossItem()
        {
            string combinedName = _listItemType[rng.Next(_listItemType.Length)] + " of " + _listItemSuffix[rng.Next(_listItemSuffix.Length)];
            int minAttackValue = rng.Next(15, 20);
            int maxAttackValue = rng.Next(16, 30);
            while (maxAttackValue == minAttackValue)
            {
                maxAttackValue = rng.Next(16, 30);
            }
            return new Item(combinedName, minAttackValue, maxAttackValue, 100);

        }

        /// <summary>
        ///  Method for item generation || NOT FINAL, CHANGES YET TO BE MADE
        /// </summary>
        /// <returns></returns>
        public Item GenerateItem()
        {
            /// Method that generates a custom item
            /// Picks different values from each of the array.
            /// eg Type = [Textbook, Pen, Bag]
            /// Damage values = [1,5,9]
            /// And more to come
            /// 
            string combinedName = _listItemType[rng.Next(_listItemType.Length)] + " of " + _listItemSuffix[rng.Next(_listItemSuffix.Length)];
            // Add level stacking
            int minAttackValue = rng.Next(5, 10);
            int maxAttackValue = rng.Next(5, 10) + minAttackValue;
            while (maxAttackValue == minAttackValue)
            {
                maxAttackValue = rng.Next(5, 10) + minAttackValue;
            }

            // Default drop chance needs to be adjusted per level
            return new Item(combinedName, minAttackValue, maxAttackValue, rng.Next(80, 100));
        }

        /// <summary>
        /// Method for getting an item from the list
        /// </summary>
        /// <returns></returns>
        public Item AcquireItem(int iterationNum)
        {
            // Generates a random number based on itemList size
            if (rng.Next(0, 100) <= ItemList[iterationNum].DropChance)
            {
                return ItemList[iterationNum];
            }
            return null;
        }

        /// <summary>
        ///  Method that displays all the items in the inventory.
        /// </summary>
        //public void DisplayInventory()
        //{
        //    foreach (Item i in ItemList)
        //    {

        //        Console.WriteLine(i);
        //    }
        //    return;
        //}
    }
}
