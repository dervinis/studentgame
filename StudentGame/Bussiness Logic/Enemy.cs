﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudentGame
{
    public class Enemy : Entity
    {
        static Random rng = new Random();

        List<Item> _dropList;

        /// <summary>
        /// Tables of values, each value is picked randomly when needed
        /// </summary>
        string[] _listOfEnemyNames = new string[3] { "Bully", "Punk", "Student" };
        int[] _listOfCurrency = new int[6] { 10, 20, 30, 40, 50, 60 };

        internal List<Item> DropList
        {
            get
            {
                return _dropList;
            }

            set
            {
                _dropList = value;
            }
        }



        /// <summary>
        /// Constructor for enemy object
        /// </summary>
        /// <param name="etName"></param>
        /// <param name="etHealth"></param>
        /// <param name="minDmg"></param>
        /// <param name="maxDmg"></param>
        /// <param name="exp"></param>
        /// <param name="lvl"></param>
        /// <param name="inv"></param>
        public Enemy(byte stageNumber) : base("", 0, 0, 0, 0, 0, 0, 0, 0, 0, null)
        {
            EntityName = _listOfEnemyNames[rng.Next(_listOfEnemyNames.Length)];
            GenerateEnemyStats(stageNumber);
            Defence = 5 + (stageNumber + 5);
            Accuracy = 60 + rng.Next(0, 5);
            Experience = 100 + (stageNumber * 100);
            Level = 1 + stageNumber;
            Currency = _listOfCurrency[rng.Next(_listOfCurrency.Length)];
            Luck = 10 + rng.Next(stageNumber);
            Inventory = new Inventory(2);
            DropList = new List<Item>();
        }

        /// <summary>
        /// Method that returns experience value to the player.
        /// </summary>
        /// <returns></returns>
        public int DropExperience()
        {
            // Check if enemy health is equal less than 0
            if (EntityHealth <= 0)
            {
                // Drop experience
                return Experience;
            }
            // If the entity health value is more than 0, then it returns 0.
            return 0;
        }

        /// <summary>
        /// Method that generates stats for the enemy
        /// </summary>
        public void GenerateEnemyStats(byte stageNumber)
        {
            EntityHealth = 50 + (5 * rng.Next(0, stageNumber));
            MinimumDamage = 1 + (2 * rng.Next(0, stageNumber));
            MaximumDamage = 1 + (3 * rng.Next(0, stageNumber)) + MinimumDamage;
            while (MaximumDamage == MinimumDamage)
            {
                MaximumDamage = rng.Next(1, 3) + MinimumDamage;
            }
        }

        /// <summary>
        /// Method that returns an Item held by the entity.
        /// </summary>
        /// <param name="enemInv"></param>
        /// <returns></returns>
        public void DropItemsHeld()
        {
            /// If the entity health is below or equals to 0, the entity returns(drops) a single
            /// Item from the inventory.
            /// 
            Item itemHolder;
            for (int i = 0; i < Inventory.ItemList.Count; i++)
            {
                itemHolder = Inventory.AcquireItem(i);
                if (itemHolder != null)
                {
                    DropList.Add(itemHolder);
                }
            }
        }

        //public override string ToString()
        //{
        //    string s = "Entity Name: " + EntityName + "\nEntity Health: " + EntityHealth + "\nMinimum Damage: " + MinimumDamage +
        //        "\nMaximum Damage: " + MaximumDamage + "\nDefence: " + Defence + "\nAccuracy: " + Accuracy
        //        + "\nExperience: " + Experience + "\nLevel: " + Level + "\nCurrency: " + Currency + "\nLuck: " + Luck;
        //    return s;
        //}
    }
}
