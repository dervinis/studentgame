﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentGame
{
    public class Item
    {
        /// <summary>
        /// Name of the item, displayed when requested
        /// </summary>
        private string _itemName;

        /// <summary>
        /// Minimum attack damage value of the item
        /// </summary>
        private int _minimumAttack;

        /// <summary>
        /// Maximum attack damage value of the item
        /// </summary>
        private int _maximumAttack;

        /// <summary>
        /// Drop chance of a given item
        /// </summary>
        private int _dropChance;

        private bool _equipped;

        /// <summary>
        /// Properties for each of the field variable
        /// </summary>
        public string ItemName
        {
            get
            {
                return _itemName;
            }

            set
            {
                _itemName = value;
            }
        }

        public int MinimumAttack
        {
            get
            {
                return _minimumAttack;
            }

            set
            {
                _minimumAttack = value;
            }
        }

        public int MaximumAttack
        {
            get
            {
                return _maximumAttack;
            }

            set
            {
                _maximumAttack = value;
            }
        }

        public int DropChance
        {
            get
            {
                return _dropChance;
            }

            set
            {
                _dropChance = value;
            }
        }

        public bool Equipped
        {
            get
            {
                return _equipped;
            }

            set
            {
                _equipped = value;
            }
        }

        /// <summary>
        /// Constructor for Item object
        /// </summary>
        /// <param name="iName"></param>
        /// <param name="minAtt"></param>
        /// <param name="maxAtt"></param>
        public Item(string iName, int minAtt, int maxAtt, int dropChance)
        {
            _itemName = iName;
            _minimumAttack = minAtt;
            _maximumAttack = maxAtt;
            _dropChance = dropChance;
            _equipped = false;
        }

        /// <summary>
        /// Method that displays the description of an item
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Equipped == true)
            {
                return "Item: " + ItemName + "\tEQUIPPED" + "\nDamage: " + MinimumAttack + " - " + MaximumAttack;
            }
            else
            {
                return "Item: " + ItemName + "\nDamage: " + MinimumAttack + " - " + MaximumAttack;
            }
        }


    }
}
