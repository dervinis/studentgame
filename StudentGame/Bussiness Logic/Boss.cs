﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudentGame
{
    public class Boss : Entity
    {

        static Random rng = new Random();

        private List<Item> _dropList;

        private string[] possibleBossNames = new string[5] { "Tom", "Jaimie", "Todd", "Jonas", "Malcolm" };
        private string[] possibleBossSuffixes = new string[4] { "Principle", "Teacher", "Janitor", "Dean" };

        /// <summary>
        /// field variable that gets updated whenever player advances a stage
        /// </summary>
        private byte _stageNumber;

        /// <summary>
        /// Constructor for boss object
        /// </summary>
        /// <param name="stageSet"></param>
        public Boss(byte stageNumber) : base("", 300, 20, 30, 20, 95, 2000, 5, 100, 95, null) // Null for now
        {
            string combinedName = possibleBossNames[rng.Next(possibleBossNames.Length)] + " The " + possibleBossSuffixes[rng.Next(possibleBossSuffixes.Length)];

            if (stageNumber == 10)
            {
                EntityName = combinedName;
                EntityHealth = 200;
                MinimumDamage = rng.Next(5, 20);
                MaximumDamage = rng.Next(5, 20) + MinimumDamage;
                while (MinimumDamage == MaximumDamage)
                {
                    MaximumDamage = rng.Next(5, 20) + MinimumDamage;
                }
                Defence = 10;
                Accuracy = rng.Next(70, 90);
                Experience = 1000;
                Level = 10;
                Currency = 100f;
                Luck = 10;
                Inventory = new Inventory(3);
                DropList = new List<Item>();
            }
            else if (stageNumber == 20)
            {
                EntityName = combinedName;
                EntityHealth = 400;
                MinimumDamage = rng.Next(10, 40);
                MaximumDamage = rng.Next(10, 40) + MinimumDamage;
                while (MinimumDamage == MaximumDamage)
                {
                    MaximumDamage = rng.Next(10, 40) + MinimumDamage;
                }
                Defence = 40;
                Accuracy = rng.Next(80, 95);
                Experience = 4000;
                Level = 30;
                Currency = 200f;
                Luck = 20;
                Inventory = new Inventory(3);
                DropList = new List<Item>();
            }
            else
            {
                Debug.Assert(false, "There is an error with boss generation!");
            }
        }

        public byte StageNumber
        {
            get
            {
                return _stageNumber;
            }

            set
            {
                _stageNumber = value;
            }
        }

        internal List<Item> DropList
        {
            get
            {
                return _dropList;
            }

            set
            {
                _dropList = value;
            }
        }

        /// <summary>
        /// Method that returns experience value to the player.
        /// </summary>
        /// <returns></returns>
        public int DropExperience()
        {
            // Check if boss health is equal less than 0
            if (EntityHealth <= 0)
            {
                // Drop experience
                return Experience;
            }
            // If the entity health value is more than 0, then it returns 0.
            return 0;
        }

        /// <summary>
        /// Method that returns an Item held by the entity.
        /// </summary>
        /// <param name="bossInv"></param>
        /// <returns></returns>
        public Item DropItemHeld(Inventory bossInv)
        {
            /// If the entity health is below or equals to 0, the entity returns(drops) a single
            /// Item from the inventory.
            if (EntityHealth <= 0)
            {
                Item itemHolder;
                for (int i = 0; i < Inventory.ItemList.Count; i++)
                {
                    itemHolder = Inventory.AcquireItem(i);
                    if (itemHolder != null)
                    {
                        DropList.Add(itemHolder);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Method that returns a string of a message on kill.
        /// </summary>
        /// <returns></returns>
        public string ShowMessageOnKill()
        {
            // Make a list/array of available messages to display
            // Use random to pick a random message,
            // Eg. deathMessages = ["You fail, Try Again Next time"]
            // Return deathMessages[rng]
            return null;
        }

        public override string ToString()
        {
            return "Name: " + EntityName + "\nHealth: " + EntityHealth;
        }

    }
}
