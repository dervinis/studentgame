﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StudentGame
{
    /// <summary>
    /// Enum for difficulty of the game
    /// </summary>
    enum Difficulty
    {
        EASY_MODE = 1,
        MEDIUM_MODE,
        HARD_MODE

    }

    public class Game
    {
        /// <summary>
        /// List for the enemies.
        /// </summary>
        private List<Enemy> _enemyList;

        /// <summary>
        /// List for the bosses.
        /// </summary>
        private List<Boss> _bossList;

        /// <summary>
        /// Player object for the game
        /// </summary>
        private Player player;

        /// <summary>
        /// Stage number for the game progression
        /// </summary>
        private byte stageNumber = 10;
        string name = "name";

        /// <summary>
        /// Random number generator for many random generations.
        /// </summary>
        static Random rng = new Random();

        /// <summary>
        /// Properties for the class
        /// </summary>
        public List<Enemy> EnemyList
        {
            get
            {
                return _enemyList;
            }

            set
            {
                _enemyList = value;
            }
        }

        public List<Boss> BossList
        {
            get
            {
                return _bossList;
            }

            set
            {
                _bossList = value;
            }
        }

        public byte StageNumber
        {
            get
            {
                return stageNumber;
            }

            set
            {
                stageNumber = value;
            }
        }

        /// <summary>
        /// Constructor for the game objects
        /// </summary>
        public Game()
        {
            _enemyList = new List<Enemy>();
            _bossList = new List<Boss>();
            player = CreateCharacter(name);
            _enemyList = CreateEnemyList(stageNumber);

        }

        public List<Enemy> CreateEnemyList(byte stageNumber)
        {
            for (int i = 0; i < rng.Next(3, 7); i++)
            {
                _enemyList.Add(new Enemy(stageNumber));
            }
            return _enemyList;
        }

        public Player CreateCharacter(string name)
        {
            return new Player(name);
        }

        public void GameProgress(string name)
        {
            CreateCharacter(name);
            _enemyList = CreateEnemyList(stageNumber);
            //Console.WriteLine(player.MinimumDamage);
            //Console.WriteLine(player.MaximumDamage);
            int playerDmg;
            while (_enemyList.Count > 0)
            {
                while (_enemyList[_enemyList.Count - 1].EntityHealth > 0)
                {
                    playerDmg = rng.Next(player.MinimumDamage - 1, player.MaximumDamage + 1);
                    // Enemy damage
                    _enemyList[_enemyList.Count - 1].EntityHealth -= playerDmg;
                    //Console.WriteLine("Damage: " + playerDmg);
                    //Console.WriteLine(_enemyList[_enemyList.Count - 1].EntityHealth);
                    //Console.WriteLine(_enemyList.Count);
                    //Console.ReadKey();
                }
                _enemyList.RemoveAt(_enemyList.Count - 1);
            }
            //Console.WriteLine("You have finished the stage");
        }

        public bool DeleteCharacter(string name)
        {
            // Take in name
            // Check if the name is there
            // If not false
            // If yes delete.
            return false;
        }

        public bool SaveProgress(string name, int level, int stage)
        {
            // Method saves following parameters to a file
            return false;
        }

        public bool LoadProgress(string name, int level, int stage)
        {
            /// Will be changed a lot
            return false;
        }

        /// <summary>
        /// Method that Exits the program
        /// </summary>
        public void Exit()
        {
            //System.Windows.Forms.Application.Exit();
        }


    }
}
