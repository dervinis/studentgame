﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace StudentGame.SaveFiles
{
    public enum SaveFileFormat
    {
        TXT
    }

    public class SaveStorage
    {
        //list of saved players
        protected List<Player> _playerList;

        //path to the location where the player data will be stored
        protected string _saveDirPath;

        //public PlayerSave(SaveFileFormat fileformat)
        //{
        //    //save list gets provided
        //    _playerList = null;

        //    //check and creates a file folder in local repository
        //    _saveDirPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "Saved Players", fileformat.ToString());

        //    if(Directory.Exists(_saveDirPath) == false)
        //    {
        //        Directory.CreateDirectory(_saveDirPath);
        //    }


        //}

        public List<Player> PlayerList
        {
            set { _playerList = value; }
        }

        public string SaveDirPath
        {
            get { return _saveDirPath; }
        }

        //public virtual void Save()
        //{
        //    foreach (Player plyer in _playerList)
        //    {
        //        string savePath = $"{_saveDirPath} //plyer{plyer.EntityName}.dat";

        //        using (StreamWriter writer = new StreamWriter(new FileStream(savePath, FileMode.Create))
        //        {
        //            //gets the type of file
        //            writer.WriteLine(plyer.GetType().ToString());

        //            plyer.Save(writer);
        //        }
        //    }
        //}

        //public virtual void Load()
        //{

        //}
    }
}
